import os
import csv
from PIL import Image
import shutil
import ghostscript
import locale
import time
import concurrent.futures
import requests
import json
import configparser
import boto3
import re
import glob
from collections import Counter, defaultdict
import sys
import pandas as pd
from tabulate import tabulate
#import pdb; pdb.set_trace()
doc_name = '56627995_merged'
def process_segments(file):
    #import pdb; pdb.set_trace()
    pages_data = image_cropping(file)
    print("*****TOTAL PAGES:- %s*****" %str(len(pages_data)))
    # Creating json file here
    if not os.path.isfile('json_files/%s.json' %file.rsplit('.', 1)[0]):
        with open('json_files/%s.json' %file.rsplit('.', 1)[0], 'w', encoding='utf-8') as f:
            json.dump(pages_data, f, ensure_ascii=False, indent=4)
def image_cropping(pdfname):
    #import pdb; pdb.set_trace()
    dirname = 'temp\\%s' %pdfname.rsplit('.')[0]
    if not os.path.exists("E:\\Practice\\spliter\\" + dirname):
        os.mkdir("E:\\Practice\\spliter\\" + dirname)
    else:
        pass
    # get pages text here
    pages_data = get_pages_data(pdfname, dirname)
    return pages_data
def get_pages_data(pdfname, dirname):
    pages_data = {}
    # check here if the text is already present
    if os.path.isfile('json_files/%s.json' %pdfname.rsplit('.', 1)[0]):
        f = open('json_files/%s.json' %pdfname.rsplit('.', 1)[0], encoding='utf-8')
        pages_data = json.load(f)
    else:
        pdf2png(pdfname, dirname)
        dir_files = os.listdir(dirname)
        filelimit = len(dir_files)
        data_json = {'filename': pdfname}
        #import pdb; pdb.set_trace()
        for page in range(1, filelimit + 1):
            filename = "page-" + str(page) + ".png"
            # response = im_ocr(r'%s\%s'%(dirname, filename))
            # lineitems, worditems = write_texts(response, filename, dirname)

            #pages_data[filename.split('.')[0]] = worditems

    print("\n\n***OCR Completed***\n\n")
    return pages_data
def pdf2png(filename, dirname):
    args = ["pef2png", # actual value doesn't matter
            "-dNOPAUSE",
            "-sDEVICE=png16m",
            "-r144",
            "-sOutputFile=%s" %(dirname + '\\page-%01d.png'),
            inputpath + filename]

    encoding = locale.getpreferredencoding()
    args = [a.encode(encoding) for a in args]
    with ghostscript.Ghostscript(*args) as g:
        ghostscript.PleaseDisplayUsage()
inputpath = 'files/'
files_list = os.listdir(inputpath)
title_list = []
bkpg_list = []
docno_list = []
final_result = {'chunks': []}
ap = []
print("*****Initializing Document Separator*****")
for file in files_list:
    if doc_name in file:
        #import pdb; pdb.set_trace()
        result, pages_data, title_json, doc_json, bkpg_data = process_segments(file)
        total_pages = len(pages_data.keys())
        title_json = {k: v for k, v in title_json.items() if v not in ['note', 'REQUEST FOR NOTICE OF DEFAULT', 'termination']}
        res = []
        for i in result:
            il = []
            for j in i:
                il.append(int(j))
            res.append(il)