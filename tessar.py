import cv2
import pytesseract
import glob
import pdb
from pytesseract import Output
import pdf2image
import  pandas as pd
from pdf2image import convert_from_path
workpdf = '56627995_merged.pdf'
images = convert_from_path(f'E:\Practice\spliter\input_files\{workpdf}',poppler_path=r'E:\Practice\poppler-22.01.0\Library\bin')
for i in range(len(images)):
    images[i].save(r'temp\page' + str(i) + '.jpg', 'JPEG')
textDict = {}
csvlist = []
pytesseract.pytesseract.tesseract_cmd=r'C:\Program Files\Tesseract-OCR\tesseract.exe'
Image_list = glob.glob(r"E:\Practice\spliter\temp\\*.jpg")
#Image_list = glob.glob('page0.jpg')
for img in Image_list:
    image = cv2.imread(img)
    words_dict = pytesseract.image_to_data(image,output_type=Output.DICT)
    filename = img.split('\\')
    filename = filename[4].replace('.jpg','')
    # f = open(f"Text_fromImage\img{filename}.txt", "w")
    # textDict[f"{filename}"] = text
    # f.write(text)
    # f.close()
    temp = []
    new_txt = ""
# pdb.set_trace()
# for item in text["text"]:
#     if item == '':
#         temp.append(new_text)
#         new_text = ""
#     else:
#         new_text = new_text + ' ' + item

    text = ' '.join(i for i in words_dict['text']).strip().split(' ')
    words = []
    for ind, txt in enumerate(text):
        if txt:
            if not new_txt:
                new_txt = txt
            else:
                new_txt = new_txt + ' ' + txt

            if ind == len(text) - 1:
                words.append(new_txt)
        else:
            if new_txt not in words:
                words.append(new_txt)
                new_txt = ''
    print(f'processing-----{filename}')
    words_list = []
    for ind, val in enumerate(words_dict['text']):
        tmp = []
        if val:
            tmp.append([words_dict['text'][ind], words_dict['left'][ind],
                        words_dict['top'][ind], words_dict['width'][ind], words_dict['height'][ind]])
            words_list.extend(tmp)
    final_dict = {}
    for i in words:
        if i:
            for k in words_list:
                if i.lower().startswith(k[0].lower()):
                    if i not in final_dict:
                        final_dict[i] = k


    text = [i for i in final_dict.keys()]
    left = [i[1] for i in final_dict.values()]
    top = [i[2] for i in final_dict.values()]
    width = [i[3] for i in final_dict.values()]
    height = [i[4] for i in final_dict.values()]

    df = pd.DataFrame({'Text':text,
                  'Left':left,
                  'top':top,
                  'Width':width,
                  'Height':height
                  })
    #df.to_csv(f'CSVs\image_to_data_{filename}.csv',index= False)
    csvlist.append(df)
    print('df appended')
with pd.ExcelWriter('Merged_Excel.xlsx') as writer:
    for ind, dfs in enumerate(csvlist):
        dfs.to_excel(writer, sheet_name=f'sheet_{ind + 1}',index=False)



#
print('pd merged')

# words_list = []
# lines_dict = {}
# for ind, val in enumerate(words_dict['text']):
    # tmp = []
    # if val:
    #     if
        # tmp.append([words_dict['text'][ind], words_dict['left'][ind],
        #             words_dict['top'][ind], words_dict['width'][ind], words_dict['height'][ind]])
        # words_list.extend(tmp)
# index_of_text = []
# for item in temp:
#     if item!='':
#         item_index = temp.index(item)
#         index_of_text.append(item_index)
#     else:
#         index_of_text.append(" ")
# height_list = []
# Text_list  =[]
# height_list_inCsv= []
# Text_position_Top = []
# pdb.set_trace()
# for digit in index_of_text:
#     if digit!=' ':
#         for one_text in temp:
#             if int(digit)==temp.index(one_text):
#                 Text_list.append(one_text)
#                 height_list_inCsv.append(text["height"][digit])
#                 Text_position_Top.append(text["top"][digit])
#                 break
# height_of_text = []
# pdb.set_trace()
# dict = {'Texts': Text_list, 'Height' : height_list_inCsv,}
# df = pd.DataFrame(dict)
# df.to_csv('Textandheight.csv')
# print('done')




