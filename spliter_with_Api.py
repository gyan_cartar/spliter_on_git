import cv2
import pytesseract   # OCR
import glob
import pdb
from pytesseract import Output
from pytesseract import image_to_string
import pdf2image   #converts pdf to image
import  pandas as pd
from pdf2image import convert_from_path
workpdf = '202112200005.pdf'
from PIL import Image
import os
from flask import Flask, request

app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        rd = request.files.to_dict()['key']
        filename = rd.filename
        if os.path.isfile(f'E:\Practice\spliter\input_files\{filename}'):
            print('File Present Processing OCR....')
        else:
            rd.save(f'E:\Practice\spliter\input_files\{filename}')
            print ("file downloaded sucsesfully, OCR processing....")
    import pdb; pdb.set_trace()
    images = convert_from_path(f'E:\Practice\spliter\input_files\{filename}',poppler_path=r'E:\Practice\poppler-22.01.0\Library\bin')
    import pdb; pdb.set_trace()
    for i in range(len(images)):
        images[i].save(r'temp\page' + str(i) + '.jpg', 'JPEG')
    textDict = {}
    csvlist = []
    pytesseract.pytesseract.tesseract_cmd=r'C:\Program Files\Tesseract-OCR\tesseract.exe'
    import pdb; pdb.set_trace()
    Image_list = glob.glob(r"E:\Practice\spliter\temp\\*.jpg")
    for img in Image_list:
        image = cv2.imread(img)
        words_dict = pytesseract.image_to_data(image,output_type=Output.DICT)
        filename = img.split('\\')
        filename = filename[4].replace('.jpg','')
        temp = []
        new_txt = ""

        text = ' '.join(i for i in words_dict['text']).strip().split(' ')
        words = []
        # import pdb; pdb.set_trace()
        for ind, txt in enumerate(text):
            if txt:
                if not new_txt:
                    new_txt = txt
                else:
                    new_txt = new_txt + ' ' + txt

                if ind == len(text) - 1:
                    words.append(new_txt)
            else:
                if new_txt not in words:
                    words.append(new_txt)
                    new_txt = ''
        # import pdb; pdb.set_trace()
        print(f'processing-----{filename}')
        words_list = []
        for ind, val in enumerate(words_dict['text']):
            tmp = []
            if val:
                tmp.append([words_dict['text'][ind], words_dict['top'][ind],
                            words_dict['height'][ind]])
                words_list.extend(tmp)
        final_dict = {}
        for i in words:
            if i:
                # import pdb; pdb.set_trace()
                for k in words_list:
                    if i.lower().startswith(k[0].lower()):
                        if i not in final_dict:
                            final_dict[i] = k


        text = [i for i in final_dict.keys()]
        top = [i[1] for i in final_dict.values()]
        height = [i[2] for i in final_dict.values()]
        # with open('D:/Practice/splitter/temp/readme.txt', 'w') as f:
        #     f.write(text)

        df = pd.DataFrame({'Text':text,
                      'top':top,
                      'Height':height
                      })
        #df.to_csv(f'CSVs\image_to_data_{filename}.csv',index= False)
        csvlist.append(df)
        print('df appended')
    with pd.ExcelWriter('Merged_Excel.xlsx') as writer:
        for ind, dfs in enumerate(csvlist):
            dfs.to_excel(writer, sheet_name=f'sheet_{ind + 1}',index=False)
    return 'Done'
if __name__ == '__main__':
    app.run(debug = True)
