import cv2
import pytesseract
import glob
import pdb
from pytesseract import Output
import pdf2image
import  pandas as pd
from pdf2image import convert_from_path
workpdf = '56627995_merged.pdf'
images = convert_from_path(f'E:\Practice\spliter\input_files\{workpdf}',poppler_path=r'E:\Practice\poppler-22.01.0\Library\bin')
for i in range(len(images)):
    images[i].save(r'temp\page' + str(i) + '.jpg', 'JPEG')
textDict = {}
pytesseract.pytesseract.tesseract_cmd=r'C:\Program Files\Tesseract-OCR\tesseract.exe'
Image_list = glob.glob('page0.jpg')
for img in Image_list:
    image = cv2.imread(img)
    text = pytesseract.image_to_data(image,output_type=Output.DICT)
    # filename = img.split('\\')
    # filename = filename[4].replace('.jpg','')
    # f = open(f"Text_fromImage\img{filename}.txt", "w")
    # textDict[f"{filename}"] = text
    # f.write(text)
    # f.close()
temp = []
new_text = ""
for item in text["text"]:
    if item == '':
        temp.append(new_text)
        new_text = ""
    else:
        new_text = new_text + ' ' + item
if new_text != "":
    temp.append(new_text)
print(temp)
index_of_text = []
for item in temp:
    if item!='':
        item_index = temp.index(item)
        index_of_text.append(item_index)
    else:
        index_of_text.append(" ")
height_list = []
Text_list  =[]
height_list_inCsv= []
Text_position_Top = []
for digit in index_of_text:
    if digit!='':
        height_list.append(digit)
        for one_text in temp:
            if int(digit)==temp.index(one_text):
                Text_list.append(one_text)
                height_list_inCsv.append(text["height"][digit])
                Text_position_Top.append(text["top"][digit])
                break
height_of_text = []
dict = {'Texts': Text_list, 'Height' : height_list_inCsv,}
df = pd.DataFrame(dict)
df.to_csv('Textandheight.csv')
print('done')




